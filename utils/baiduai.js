/**
 * 调用百度EasyDL、OCR示例代码
 * 只提供了一个EasyDL、OCR接口的封装。可以根据自己的需求封装即可
 */
let accessToken = ''//自己的accessToken 根据实际情况可以进行封装 自动获取token
let easydlUrl = 'https://aip.baidubce.com/rpc/2.0/ai_custom/v1/classification/handwritenum';//替换自己训练且已经发布的接口地址
let ocrGenerallUrl = 'https://aip.baidubce.com/rest/2.0/ocr/v1/general_basic';//OCR通用识别接口
let faceDetectUrl = 'https://aip.baidubce.com/rest/2.0/face/v3/detect';//人脸检测V3版本
let sameAddUrl = 'https://aip.baidubce.com/rest/2.0/realtime_search/same_hq/add';//相同图片搜索—入库
let sameSearchUrl = 'https://aip.baidubce.com/rest/2.0/realtime_search/same_hq/search';//相同图片搜索—检索
let faceDetectRequest = (detectData,image_type,face_field,callback)=>{
  //拼接接口body参数
  let params = {
    image:detectData,
    image_type:image_type,
    face_field:face_field,
    max_face_num:10
  }
  //发送接口请求
  wx.request({
    url: faceDetectUrl + '?access_token=' + accessToken,
    data:params,
    header: {
      'content-type': 'application/json'
    },
    method: 'POST',
    success:function(res){
      callback.success(res.data)
    },
    fail: function (res) {
      if (callback.fail)
        callback.fail()
    }
  })
}
//EasyDL接口 图片数据 返回结果条数 根据物体 分类 文本 请修改第二个参数哦
let easyDLRequest = (base64Img,topNum,callback)=>{
  //拼接接口body参数
  let params = {
     top_num:topNum,
     image:base64Img
  }
  //发送接口请求
  wx.request({
    url: easydlUrl + '?access_token=' + accessToken,
    data:params,
    header: {
      'content-type': 'application/json'
    },
    method: 'POST',
    success:function(res){
      callback.success(res.data)
    },
    fail: function (res) {
      if (callback.fail)
        callback.fail()
    }
  })
}
//OCR通用识别接口 图片base64 只是简单示例 别的参数如何封装建议自己学习一下JavaScript
let ocrGeneralRequestByImage = (base64Img,callback) => {
  //拼接接口body参数
  let params = {
    detect_direction:true,
    image: base64Img
  }
  //发送接口请求
  wx.request({
    url: ocrGenerallUrl + '?access_token=' + accessToken,
    data: params,
    header: {
      'content-type': 'application/x-www-form-urlencoded'
    },
    method: 'POST',
    success: function (res) {
      callback.success(res.data)
    },
    fail: function (res) {
      if (callback.fail)
        callback.fail()
    }
  })
}
//相同图片搜索—入库接口简单封装
let sameAddlRequestByImage = (base64Img,brief,callback) => {
  //拼接接口body参数
  let params = {
    image: base64Img,
    brief:brief
  }
  //发送接口请求
  wx.request({
    url: sameAddUrl + '?access_token=' + accessToken,
    data: params,
    header: {
      'content-type': 'application/x-www-form-urlencoded'
    },
    method: 'POST',
    success: function (res) {
      callback.success(res.data)
    },
    fail: function (res) {
      if (callback.fail)
        callback.fail()
    }
  })
}
//相同图片搜索—检索接口简单封装
let sameSearchRequestByImage = (base64Img,callback) => {
  //拼接接口body参数
  let params = {
    image: base64Img
  }
  //发送接口请求
  wx.request({
    url: sameSearchUrl + '?access_token=' + accessToken,
    data: params,
    header: {
      'content-type': 'application/x-www-form-urlencoded'
    },
    method: 'POST',
    success: function (res) {
      callback.success(res.data)
    },
    fail: function (res) {
      if (callback.fail)
        callback.fail()
    }
  })
}
//暴露出去的接口
module.exports = {
  easyDLRequest: easyDLRequest,
  ocrGeneralRequestByImage: ocrGeneralRequestByImage,
  faceDetectRequest:faceDetectRequest,
  sameSearchRequestByImage:sameSearchRequestByImage,
  sameAddlRequestByImage:sameAddlRequestByImage
}