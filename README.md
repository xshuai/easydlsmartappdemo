# easydlsmartappdemo
## 声明说明
很多人说跑不通。首先声明。小帅丶都是亲测没问题后才分享的代码。
微信小程序建议自己都去看一下文档。小帅丶只是在巨人肩膀上搞一下。你们都不愿意去了解一下巨人的产品文档介绍。就直接认为是代码的问题。表示无奈。
本地开发工具测试之前。把安全域名校验取消掉。不想去掉。就去小程序平台自己把接口域名添加进去。
## 作者QQ:783021975
#### 项目介绍
微信小程序 调用百度AI 文字识别、EasyDL接口、人脸检测接口
无需后台处理图片示例代码

目前demo为调用 **EasyDL、文字识别(通用)、人脸检测接口(V3)**  接口示例

#### 代码使用说明

##### AccessToken如何获取(百度AI官网图文教程) 

[https://ai.baidu.com/docs#/Auth/e72c8d67](https://ai.baidu.com/docs#/Auth/e72c8d67)

修改/utils/baiduai.js 中以下部分代码:

```
let accessToken = ''//自己的accessToken 根据实际情况可以进行封装 自动获取token
let easydlUrl = 'https://aip.baidubce.com/rpc/2.0/ai_custom/v1/classification/XXX';//替换自己训练且已经发布的接口地址
let ocrGenerallUrl = 'https://aip.baidubce.com/rest/2.0/ocr/v1/general_basic';//OCR通用识别接口
let faceDetectUrl = 'https://aip.baidubce.com/rest/2.0/face/v3/detect';//人脸检测V3版本
```
运行即可。记得开发者工具不校验HTTP域名哦。

如需要测试其他接口。参考baiduai.js中的代码修改即可。
在调用的页面JS中记得修改即可。

 **页面Demo为index、ocr目录。**

#### EasyDL示例图

![EasyDL示例图](https://ai.bdstatic.com/file/8AAF926D05C349798947B4A7B18C33A4 "EasyDL示例图")


#### OCR示例图

![OCR示例图](https://images.gitee.com/uploads/images/2019/0115/094036_0d0825ad_131538.jpeg "OCR示例图")


#### 作者的个人小程序:小帅一点资讯

![微信扫一下 体验AI好玩的图像处理](https://images.gitee.com/uploads/images/2018/1123/153613_bca3be13_131538.jpeg "微信扫一下 体验AI好玩的图像处")

#### 人脸检测框选示意图
![人脸检测框选示意图](https://images.gitee.com/uploads/images/2019/0815/163253_cd2ee2da_131538.png "人脸检测框选示意图")
![人脸检测框选示意图](https://images.gitee.com/uploads/images/2019/0815/163312_ecf897da_131538.png "人脸检测框选示意图")


#### 小程序顶部导航渐变色效果图带天气
![小程序顶部导航渐变色效果图带天气](https://images.gitee.com/uploads/images/2019/0925/155925_cf9596bb_131538.gif "小程序顶部导航渐变色效果图带天气")

#### 相同图片搜索-入库
![相同图片搜索-入库](https://images.gitee.com/uploads/images/2019/1220/113727_f39c0b9e_131538.gif "入库.gif")

#### 相同图片搜索-检索
![相同图片搜索-检索](https://images.gitee.com/uploads/images/2019/1220/113757_1a6554f4_131538.gif "检索.gif")
